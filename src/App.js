
import React from 'react';

import Header from './modules/Header';
import Main from './modules/Main';


import './App.css';


const App = () => {
  return (
    <div className="App">
      <Header />
      <Main />
    </div>
  );
}

export default App;
