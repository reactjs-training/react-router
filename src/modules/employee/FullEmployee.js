import React from "react";
import Service from "./service";
import { Link } from "react-router-dom";

const FullEmployee = () => (
  <div>
    <ul>
      {
        Service.all().map(emp => (
          <li key={emp.id}>
            <Link to={`/employee/${emp.id}`}>{emp.name}</Link>
          </li>
        ))
      }
    </ul>
  </div>
);

export default FullEmployee;