const Service = {
  employee: [
    {
      id: 1,
      name: "Prafulla",
      age: 30,
      bloodgroup: "a"
    },
    {
      id: 2,
      name: "Nitin",
      age: 29,
      bloodgroup: "b"
    },
    {
      id: 3,
      name: "Sakshi",
      age: 33,
      bloodgroup: "c"
    },
    {
      id: 4,
      name: "Asma",
      age: 35,
      bloodgroup: "d"
    }
  ],
  all: function() { return this.employee},
  get: function(id) {
    const isPlayer = p => p.id === id
    return this.employee.find(isPlayer)
  }
}

export default Service;