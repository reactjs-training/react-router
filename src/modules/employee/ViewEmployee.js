import React from "react";
import Service from "./service";
import { Link } from "react-router-dom";

const ViewEmployee = (props) => {

  const {match: {params: {id}}} = props;

  const employee = Service.get(parseInt(id, 10));

  console.log(id);
  
  if(!employee) {
    return <div>Sorry, but the employee was not found</div>
  }

  return (
    <div>
      <h1> Name - {employee.name} - (# {employee.id})</h1>
      <h2> Age - {employee.age}</h2>
      <h2> Boold Group - {employee.bloodgroup}</h2>
      <Link to="/employee">Back</Link>
    </div>
  )


};

export default ViewEmployee;