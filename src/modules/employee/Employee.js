import React from 'react';
import { Switch, Route } from 'react-router-dom';

import FullEmployee from "./FullEmployee";
import ViewEmployee from "./ViewEmployee";

const Employee = () => (
  <Switch>
    <Route exact path="/employee" component={FullEmployee}></Route>
    <Route exact path="/employee/:id" component={ViewEmployee}></Route>
  </Switch>
);

export default Employee;