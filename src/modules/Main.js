import React from "react";

import { Switch, Route } from "react-router-dom";

import Home from "./home/Home";
import Employee from "./employee/Employee";
import About from "./about/About";


const Main = () => (
  <div>
    <Switch>
      <Route exact path="/" component={Home} ></Route>
      <Route path="/employee" component={Employee}></Route>
      <Route path="/about" component={About}></Route>
    </Switch>
  </div>
);

export default Main;